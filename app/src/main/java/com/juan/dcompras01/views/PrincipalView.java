package com.juan.dcompras01.views;

import android.app.DatePickerDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.juan.dcompras01.R;
import com.juan.dcompras01.model.manager.ProductoManager;
import com.juan.dcompras01.model.model.Producto;
import com.juan.dcompras01.presenters.IDetalleCompraPresenter;
import com.juan.dcompras01.presenters.adapters.DetalleCompraAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import butterknife.ButterKnife;

public class PrincipalView extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, FDetalleComprasView.OnFragmentInteractionListener, FComprasView.OnFragmentInteractionListener {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private SearchView searchView;

    private Menu menu;

    //registro e fragments
    private SparseArray<Fragment> registeredFragments = new SparseArray<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    mostrarOcultarMenu(1);
                } else {
                    mostrarOcultarMenu(0);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                Intent intent = new Intent(PrincipalView.this, CompraAMView.class);
                //Bundle bundle = new Bundle();
                int requestCode = 1; // Or some number you choose
                startActivityForResult(intent, requestCode);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.main_content);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ButterKnife.bind(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        //ESTE CODIGO ES PARA ACTIVAR LA BUSQUEDA DE LA BARRA DE TAREA
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String newText) {
                // this is your adapter that will be filtered
                buscarEnTab(newText);
                return true;
            }

            public boolean onQueryTextSubmit(String query) {
                //Here u can get the value "query" which is entered in the search box.
                return true;
            }
        };
        searchView.setOnQueryTextListener(queryTextListener);

        this.menu = menu;
        mostrarOcultarMenu(1);//para visualizar al inicio

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.add){
            Intent intent = new Intent(PrincipalView.this, CompraAMView.class);
            //Bundle bundle = new Bundle();
            int requestCode = 1; // Or some number you choose
            startActivityForResult(intent, requestCode);
            //startActivity(intent);
        }
        else if(id == R.id.date){
            showDialogDate();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Metoddo para mostrar el diago con el calendario para relizar busquedas en compras anteriosres
     */
    public void showDialogDate(){
        int mYear, mMonth, mDay, mHour, mMinute;

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,int monthOfYear, int dayOfMonth) {

                        FComprasView fComprasView = (FComprasView)registeredFragments.get(1);
                        if(fComprasView != null){
                            fComprasView.buscarEnLista(year, monthOfYear, dayOfMonth);
                        }
                        //Toast.makeText(view.getContext(),"-->" + year + "-" + monthOfYear + "-" + dayOfMonth,Toast.LENGTH_SHORT).show();
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.main_content);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.d_ultima_compra) {
            mViewPager.setCurrentItem(0);//seleccionar el tab cero
        } else if (id == R.id.d_compras_anteriores) {
            mViewPager.setCurrentItem(1);//seleccionar el tab uno
        }
        else if(id == R.id.d_maps_sugerir){
            Intent intent = new Intent(PrincipalView.this, MapsLSugeridosView.class);
            startActivity(intent);
        }else if (id == R.id.d_acerca_de) {
            Intent intent = new Intent(PrincipalView.this, ACercaDeView.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.main_content);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    /**
     * Metodo para mostrar ocultar menu del action bar
     * @param sw
     */
    public void mostrarOcultarMenu(int sw){
        if(sw == 1){
            this.menu.getItem(1).setVisible(true);
            this.menu.getItem(2).setVisible(false);
        }
        else {
            this.menu.getItem(1).setVisible(false);
            this.menu.getItem(2).setVisible(true);
        }

    }

    //metodo para realizar busqueda en fragment
    public void buscarEnTab(String txtBuscar){
        int current = mViewPager.getCurrentItem();
        if(current == 0){
            FDetalleComprasView fDetalleComprasView = (FDetalleComprasView)registeredFragments.get(0);
            if(fDetalleComprasView != null){
                fDetalleComprasView.buscarEnLista(txtBuscar);
            }
        }
    }

    //result de actividad secundaria
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        //String value = data.getString("someValue");
        FDetalleComprasView fDetalleComprasView = (FDetalleComprasView)registeredFragments.get(0);
        FComprasView fComprasView = (FComprasView)registeredFragments.get(1);

        if(fDetalleComprasView != null){
            fDetalleComprasView.actualizarLista();
        }
        if(fComprasView != null){
            fComprasView.actualizarLista();
        }
        //Log.i("------>",">>>>>>>>>>>>>>>>LLEGA POR AQUI RETORNO DE ACTIVITY>>>>>>>>>>>>>>>>");
    }

    //##########################################################################################################################
    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        public static Fragment newInstance(int sectionNumber) {

            Fragment fragment = null;
            switch (sectionNumber){
                case 1:
                    fragment = new FDetalleComprasView();
                    break;
                case 2:
                    fragment = new FComprasView();
                    break;
                default:
                    fragment = new FDetalleComprasView();
            }

            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_fdetalle_compra, container, false);
            return rootView;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = PlaceholderFragment.newInstance(position + 1);

            if(position == 0){
                registeredFragments.put(0,fragment);//regsitrando instancia de fragmentos para poder llamarlo despues
            }
            else if(position == 1){
                registeredFragments.put(1,fragment);
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.ultima_compra);
                case 1:
                    return getString(R.string.compras_anteriores);
            }
            return null;
        }
    }
    //##########################################################################################################################

}
