package com.juan.dcompras01.views;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.juan.dcompras01.R;
import com.juan.dcompras01.model.model.Ubicacion;
import com.juan.dcompras01.presenters.MapsSugerirPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapsSugerirView extends AppCompatActivity implements OnMapReadyCallback, IMapsSugerirView {

    private GoogleMap mMap;

    private MapsSugerirPresenter mapsSugerirPresenter;
    private Marker marker = null;

    @BindView(R.id.maps_sugerir_progress)
    View mProgressView;
    @BindView(R.id.maps_sugerir_form)
    View mMapsSugerirFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_sugerir);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ButterKnife.bind(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        double latitud = -17.389788;
        double longitud = -66.173835;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        }

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        String locationProvider = LocationManager.NETWORK_PROVIDER;
        // Or use LocationManager.GPS_PROVIDER

        if(ContextCompat.checkSelfPermission( this, android.Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED ){
            Location ubicacion = locationManager.getLastKnownLocation(locationProvider);
            latitud = ubicacion.getLatitude();
            longitud = ubicacion.getLongitude();
        }

        // Add a marker in Sydney and move the camera
        LatLng america = new LatLng(latitud, longitud);
        Marker marker = mMap.addMarker(new MarkerOptions().position(america).title(getString(R.string.mensaje_popup)));
        this.marker = marker;

        mMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(latitud,longitud) , 12.0f));

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                // TODO Auto-generated method stub
                //lstLatLngs.add(point);
                mMap.clear();
                mostrarEnOtraPosicion(new MarkerOptions().position(point));
            }
        });

        marker.showInfoWindow();

        this.mapsSugerirPresenter = new MapsSugerirPresenter(getApplicationContext(),this);

    }

    /**
     * para mostrar posicion nueva
     * @param markerOptions
     */
    private void mostrarEnOtraPosicion(MarkerOptions markerOptions){
        marker = mMap.addMarker(markerOptions);
    }

    @Override
    public void cargarUbicacion(Ubicacion ubicacion) {

    }

    @Override
    public void mostrarProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mMapsSugerirFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mMapsSugerirFormView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mMapsSugerirFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mMapsSugerirFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void mostrarMensaje(String mensaje) {
        Snackbar.make(this.mMapsSugerirFormView, mensaje, Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }

    @Override
    public void finalizar() {
        finish();
    }


    ///////////////////////////////////////////////////////////
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_compra_am, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.save_compra) {
            obtenerDescripcion();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Metodo para solicitar descripcion de la ubicacion en el mapa
     * @return
     */
    private void obtenerDescripcion(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Descripcion");

        final EditText input = new EditText(this);

        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setFocusable(true);
        input.requestFocus();
        builder.setView(input);

        builder.setPositiveButton(getString(R.string.aceptar), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String Descripcion = input.getText().toString();

                if(Descripcion.length() > 0){
                    if(marker != null){
                        Ubicacion ubicacion = new Ubicacion();
                        ubicacion.setDescripcion(Descripcion);
                        ubicacion.setLatitud(marker.getPosition().latitude + "");
                        ubicacion.setLogitud(marker.getPosition().longitude + "");
                        //Toast.makeText(getApplicationContext(), ">>>" + ubicacion.getLatitud() + " " + ubicacion.getLogitud(), Toast.LENGTH_SHORT).show();
                        mapsSugerirPresenter.guardarUbicacion(ubicacion);
                    }
                }
            }
        });
        builder.setNegativeButton(getString(R.string.cancelar), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }

}
