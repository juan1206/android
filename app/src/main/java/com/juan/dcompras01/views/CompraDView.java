package com.juan.dcompras01.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.juan.dcompras01.R;
import com.juan.dcompras01.model.model.Compra;
import com.juan.dcompras01.model.model.DetalleCompra;
import com.juan.dcompras01.presenters.CompraDPresenter;
import com.juan.dcompras01.presenters.adapters.CompraDAdapter;
import com.juan.dcompras01.util.ConstantesDcompras;
import com.juan.dcompras01.util.UtilDcompras;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CompraDView extends AppCompatActivity implements ICompraDView{
    @BindView(R.id.detalle_compra_a)
    TextView txtFechaCompra;
    @BindView(R.id.detalle_compra_a_recyclerView)
    RecyclerView recycler;

    @BindView(R.id.detalle_compra_d_progress)
    View mProgressView;
    @BindView(R.id.detalle_compra_d_form)
    View mDetalleCompraFormView;

    private CompraDPresenter compraDPresenter;
    private RecyclerView.LayoutManager lManager;
    private UtilDcompras utilDcompras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compra_d);
        compraDPresenter = new CompraDPresenter(getApplicationContext(), this);

        this.startRecycler();

        String idCompra = null;
        try {
            Bundle bundle = getIntent().getExtras();
            idCompra = bundle.getString("idCompra");
        }catch (Exception er){

        }

        ButterKnife.bind(this);

        compraDPresenter.cargarRecyclerView(idCompra);
    }

    @Override
    public void cargarRecyclerView(Compra compra, CompraDAdapter compraDAdapter) {
        this.utilDcompras = new UtilDcompras();

        txtFechaCompra.setText(getString(R.string.fecha) + ConstantesDcompras.SPACE + utilDcompras.convertirDateEnString(compra.getFecha(),ConstantesDcompras.FORMAT_DD_MM_YYYY));
        recycler.setAdapter(compraDAdapter);
        recycler.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void mostrarProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mDetalleCompraFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mDetalleCompraFormView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mDetalleCompraFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mDetalleCompraFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void mostrarMensaje(String mensaje) {
        Snackbar.make(this.mDetalleCompraFormView, mensaje, Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }

    @Override
    public void finalizar() {
        finish();
    }

    //##############iniciar recycler view
    private void startRecycler() {
        recycler = (RecyclerView)findViewById(R.id.detalle_compra_a_recyclerView);
        recycler.setHasFixedSize(true);

        lManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(lManager);
    }

    ///////////////////////////////////////////////////////////
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_compra_d, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.send_compra) {
            this.compraDPresenter.guardarDetalleCompra();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
