package com.juan.dcompras01.views;

import com.juan.dcompras01.model.model.DetalleCompra;

/**
 * Created by Juan choque on 10/12/2017.
 */

public interface IDetalleCompraAMView {
    void cargarDetalleCompra(DetalleCompra detalleCompra);;
    void mostrarMensaje(String mensaje);
    void mostrarProgress(boolean sw);
    void finalizar();
}
