package com.juan.dcompras01.views;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.juan.dcompras01.R;
import com.juan.dcompras01.model.model.Compra;
import com.juan.dcompras01.model.model.DetalleCompra;
import com.juan.dcompras01.presenters.CompraPresenter;
import com.juan.dcompras01.presenters.adapters.CompraAdapter;
import com.juan.dcompras01.presenters.adapters.DetalleCompraAdapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FComprasView extends Fragment implements ICompraView {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private RecyclerView recycler;
    private RecyclerView.LayoutManager lManager;

    private View view;

    private CompraPresenter compraPresenter;

    public FComprasView() {
        // Required empty public constructor
    }

    public static FComprasView newInstance(String param1, String param2) {
        FComprasView fragment = new FComprasView();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.view = inflater.inflate(R.layout.fragment_fcompras, container, false);
        this.compraPresenter = new CompraPresenter(this.getContext(), this);
        this.startRecycler();

        //cargando los datos desde presenter
        this.compraPresenter.cargarRecycleView();

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void cargarRecycleView(CompraAdapter compraAdapter) {
        //create new adapter
        recycler.setAdapter(compraAdapter);
        recycler.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void actualizarLista() {
        this.compraPresenter.cargarRecycleView();
    }

    @Override
    public void modificarCompra(Compra compra) {
        Intent intent = new Intent(getContext(), CompraDView.class);
        Bundle bundle = new Bundle();
        bundle.putString("idCompra", compra.getId());
        intent.putExtras(bundle);

        int requestCode = 1;
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void buscarEnLista(int anio, int mes, int dia) {
        this.compraPresenter.buscarEnLista(anio, mes, dia);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    //##########################################################################################################
    private void startRecycler() {
        recycler = (RecyclerView)view.findViewById(R.id.compras_recyclerView);
        recycler.setHasFixedSize(true);

        lManager = new LinearLayoutManager(view.getContext());
        recycler.setLayoutManager(lManager);

    }
}
