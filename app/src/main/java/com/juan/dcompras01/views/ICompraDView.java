package com.juan.dcompras01.views;

import com.juan.dcompras01.model.model.Compra;
import com.juan.dcompras01.model.model.DetalleCompra;
import com.juan.dcompras01.presenters.adapters.CompraDAdapter;

/**
 * Created by Juan choque on 10/12/2017.
 */

public interface ICompraDView {
    void cargarRecyclerView(Compra compra, CompraDAdapter compraDAdapter);
    void mostrarProgress(boolean b);
    void mostrarMensaje(String saveIdDb);
    void finalizar();
}
