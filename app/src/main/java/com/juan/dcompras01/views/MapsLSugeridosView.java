package com.juan.dcompras01.views;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.juan.dcompras01.R;
import com.juan.dcompras01.model.model.Ubicacion;
import com.juan.dcompras01.presenters.MapsLSugeridoPresenter;

import java.util.List;

public class MapsLSugeridosView extends AppCompatActivity implements OnMapReadyCallback, IMapsLSugeridoView, GoogleMap.OnMapLoadedCallback{

    private GoogleMap mMap;

    private MapsLSugeridoPresenter mapsLSugeridoPresenter;

    private static final int PERMISSION_REQUEST_CODE = 1;
    private MinDisLocationListener locationListener;
    private LocationManager lm;
    private static Double latitud = -17.389788, longitud = -66.173835;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_lsugeridos);

        FragmentManager fm = getSupportFragmentManager();

        locationListener = new MinDisLocationListener();
        lm = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 2, this.locationListener);
        }

        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 2, this.locationListener);
        //if (mMapFragment == null) {
        SupportMapFragment mMapFragment = SupportMapFragment.newInstance();
        fm.beginTransaction().replace(R.id.map, mMapFragment).commit();
        mMapFragment.getMapAsync(this);
        //}

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        }

        mMap.setOnMapLoadedCallback(this);
        mMap.getUiSettings().setZoomGesturesEnabled(true);

        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)  == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 2, this.locationListener);
        }

        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latitud, longitud)).zoom(12).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        //drawMarker(latitud, longitud);

        this.mapsLSugeridoPresenter = new MapsLSugeridoPresenter(getApplicationContext(), this);
        this.mapsLSugeridoPresenter.cargarUbicaciones();

    }



    ///////////////////////////////////////////////////////////
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_compra_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.lugar_sugerir) {
            Intent intent = new Intent(MapsLSugeridosView.this,MapsSugerirView.class);
            startActivityForResult(intent,1);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void cargarUbicaciones(List<Ubicacion> listaUbicaciones) {
        mMap.clear();
        for (int i = 0;i < listaUbicaciones.size();i++){
            Ubicacion ubicacion = listaUbicaciones.get(i);

            LatLng latLng = new LatLng(Double.parseDouble(ubicacion.getLatitud()),Double.parseDouble(ubicacion.getLogitud()));
            drawMarker(latLng.latitude, latLng.longitude, ubicacion.getDescripcion());
        }
    }

    //result de actividad secundaria
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        mapsLSugeridoPresenter.cargarUbicaciones();
    }



    @Override
    public void onMapLoaded() {

    }

    ////////////////////////////////////////test
    public void drawMarker(double lat, double lon, final String descripcion) {
        if (mMap != null) {
            MarkerOptions marker = new MarkerOptions().position(new LatLng(lat, lon)).title(descripcion).snippet("(Toque aqui para Opciones)");

            //marker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
            //CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latitud, longitud)).zoom(12).build();
            //mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    mostrarOpciones(marker);
                }
            });
            mMap.addMarker(marker);
        }
    }

    /**
     * Meotod para mostrar opciones para mostrar
     * @param marker
     */
    public void mostrarOpciones(final Marker marker){
        CharSequence colors[] = new CharSequence[] {getString(R.string.eliminar), getString(R.string.cancelar)};

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.seleccione));
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // the user clicked on colors[which]
                if(which == 0){
                    mapsLSugeridoPresenter.eliminarUbicacion(marker.getPosition().latitude, marker.getPosition().longitude, marker.getTitle());
                }
                //Toast.makeText(getApplicationContext(),">>" + which, Toast.LENGTH_SHORT).show();
            }
        });
        builder.show();
    }

    public class MinDisLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            Log.d("location", "onLocationChanged");
            drawMarker(location.getLatitude(),location.getLongitude(),"");
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.d("location", "onStatusChanged");
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.d("location", "onProviderEnabled");
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.d("location", "onProviderEnabled");
        }
    }



}
