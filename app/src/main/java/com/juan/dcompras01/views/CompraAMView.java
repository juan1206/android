package com.juan.dcompras01.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.juan.dcompras01.R;
import com.juan.dcompras01.model.model.DetalleCompra;
import com.juan.dcompras01.model.model.Producto;
import com.juan.dcompras01.presenters.CompraAMPresenter;
import com.juan.dcompras01.presenters.IDetalleCompraPresenter;
import com.juan.dcompras01.presenters.adapters.DetalleCompraAMAdapter;
import com.juan.dcompras01.presenters.adapters.ProductoAutocompleteAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CompraAMView extends AppCompatActivity implements ICompraAMView{
    @BindView(R.id.buscar_producto_detalle_compra)
    AutoCompleteTextView txtProductoAutoComplete;
    @BindView(R.id.detalle_compra_am_recyclerView)
    RecyclerView recycler;
    @BindView(R.id.btn_nuevo_detalle_compra)
    ImageButton btnNuevoDetalle;

    private RecyclerView.LayoutManager lManager;
    private ProductoAutocompleteAdapter adapter = null;
    private ArrayList<Producto> productos = null;
    private List<DetalleCompra> listadetalles = null;
    private DetalleCompraAMAdapter detalleCompraAMAdapter = null;

    private CompraAMPresenter compraAMPresenter;

    @BindView(R.id.compra_progress)
    View mProgressView;
    @BindView(R.id.compra_am_form)
    View mDetalleCompraFormView;

    //actividad principal
    private IDetalleCompraPresenter iDetalleCompraPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compra_am);

        compraAMPresenter = new CompraAMPresenter(this.getApplicationContext(),this, iDetalleCompraPresenter);

        ButterKnife.bind(this);

        productos = new ArrayList<Producto>();
        compraAMPresenter.cargarProductos();//

        txtProductoAutoComplete = (AutoCompleteTextView) findViewById(R.id.buscar_producto_detalle_compra);
        adapter = new ProductoAutocompleteAdapter(this, productos);
        txtProductoAutoComplete.setAdapter(adapter);
        txtProductoAutoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position ,long id) {
                Producto producto = productos.get(position);
                compraAMPresenter.insertarProducto(producto, listadetalles);

                txtProductoAutoComplete.setText("");
            }
        });
        //cargarAutoComplete();

        btnNuevoDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Producto producto = new Producto();
                producto.setDescripcion(txtProductoAutoComplete.getText().toString());
                compraAMPresenter.insertarProducto(producto, listadetalles);

                txtProductoAutoComplete.setText("");
            }
        });

        //mostrar detalle
        listadetalles = new ArrayList<DetalleCompra>();

        detalleCompraAMAdapter = new DetalleCompraAMAdapter(this,listadetalles,compraAMPresenter);
        recycler.setHasFixedSize(true);
        lManager = new LinearLayoutManager(getApplicationContext());
        recycler.setLayoutManager(lManager);
        recycler.setItemAnimator(new DefaultItemAnimator());

    }

    @Override
    public void insertarDetalleRecycler(DetalleCompra detalleCompra) {
        listadetalles.add(0,detalleCompra);
        recycler.setAdapter(detalleCompraAMAdapter);
    }

    @Override
    public void eliminarDetalleRecycler(DetalleCompra detalleCompra) {
        listadetalles.remove(detalleCompra);
        recycler.setAdapter(detalleCompraAMAdapter);
    }

    @Override
    public void mostrarProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mDetalleCompraFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mDetalleCompraFormView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mDetalleCompraFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mDetalleCompraFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void mostrarMensaje(String mensaje){
        Snackbar.make(this.mDetalleCompraFormView, mensaje, Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }

    @Override
    public void cargarProductos(final List<Producto> productos) {
        for(int i = 0;i < productos.size();i++){
            this.productos.add(productos.get(i));
        }
    }

    ///////////////////////////////////////////////////////////
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_compra_am, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.save_compra) {
            this.compraAMPresenter.guardarCompra(listadetalles);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void finalizar(){
        Intent intent = new Intent();
        intent.putExtra("someValue", "data");
        setResult(RESULT_OK, intent);
        PrincipalView first = (PrincipalView) getParent();
        finish();
    }

}
