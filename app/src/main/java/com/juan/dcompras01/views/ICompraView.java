package com.juan.dcompras01.views;

import com.juan.dcompras01.model.model.Compra;
import com.juan.dcompras01.presenters.adapters.CompraAdapter;

import java.util.List;

/**
 * Created by Juan choque on 10/12/2017.
 */

public interface ICompraView {
    void cargarRecycleView(CompraAdapter compraAdapter);
    void actualizarLista();
    void modificarCompra(Compra compra);
    void buscarEnLista(int anio, int mes, int dia);
}
