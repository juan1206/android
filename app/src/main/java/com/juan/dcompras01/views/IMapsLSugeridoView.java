package com.juan.dcompras01.views;

import com.juan.dcompras01.model.model.Ubicacion;

import java.util.List;

/**
 * Created by Juan choque on 10/28/2017.
 */

public interface IMapsLSugeridoView {
    void cargarUbicaciones(List<Ubicacion>listaUbicaciones);
}
