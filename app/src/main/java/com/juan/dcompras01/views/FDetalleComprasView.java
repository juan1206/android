package com.juan.dcompras01.views;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.juan.dcompras01.R;
import com.juan.dcompras01.R2;
import com.juan.dcompras01.model.model.DetalleCompra;
import com.juan.dcompras01.presenters.DetalleCompraPresenter;
import com.juan.dcompras01.presenters.IDetalleCompraPresenter;
import com.juan.dcompras01.presenters.adapters.DetalleCompraAdapter;
import com.juan.dcompras01.util.ConstantesDcompras;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FDetalleComprasView extends Fragment implements IDetalleCompraView{
    private RecyclerView recycler;
    private RecyclerView.LayoutManager lManager;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private View view;

    private DetalleCompraPresenter detalleCompraPresenter;

    @BindView(R.id.compra_am_form)
    TextView txtFechaCompra;

    public FDetalleComprasView() {
        // Required empty public constructor
    }

    public static FDetalleComprasView newInstance(String param1, String param2) {
        FDetalleComprasView fragment = new FDetalleComprasView();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.view = inflater.inflate(R.layout.fragment_fdetalle_compra, container, false);
        this.detalleCompraPresenter = new DetalleCompraPresenter(this.getContext(),this);

        //ButterKnife.bind(getContext(),view);
        this.txtFechaCompra = view.findViewById(R.id.fecha_f_compra);

        this.startRecycler();
        detalleCompraPresenter.cargarRecycleView();

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void cargarRecycleView(String fecha, DetalleCompraAdapter detalleCompraAdapter) {
        //create new adapter
        recycler.setAdapter(detalleCompraAdapter);
        recycler.setItemAnimator(new DefaultItemAnimator());

        //cargar fecha
        txtFechaCompra.setText((!fecha.isEmpty())?getString(R.string.fecha) + ConstantesDcompras.SPACE + fecha:"");
    }

    @Override
    public void cargarRecycleView(DetalleCompraAdapter detalleCompraAdapter) {
        recycler.setAdapter(detalleCompraAdapter);
        recycler.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void modificarDetalleCompra(DetalleCompra detalleCompra) {
        Bundle bundle = new Bundle();
        bundle.putString("idDetalleCompra", detalleCompra.getId());

        Intent intent = new Intent(this.getContext(),DetalleCompraAMView.class);
        intent.putExtras(bundle);

        int requestCode = 1; // Or some number you choose
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void buscarEnLista(String txtBuscar) {
        detalleCompraPresenter.cargarRecycleView(txtBuscar);
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }


    //##########################################################################################################
    private void startRecycler() {
        recycler = (RecyclerView)view.findViewById(R.id.detalle_compra_recyclerView);
        recycler.setHasFixedSize(true);

        lManager = new LinearLayoutManager(view.getContext());
        recycler.setLayoutManager(lManager);
    }

    //###########################################################################################################
    public void actualizarLista(){
        detalleCompraPresenter.cargarRecycleView();
    }
}
