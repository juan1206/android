package com.juan.dcompras01.views;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;

import com.juan.dcompras01.R;

import java.sql.BatchUpdateException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ACercaDeView extends AppCompatActivity {

    @BindView(R.id.call)
    Button btnCall;
    @BindView(R.id.whats)
    Button btnWhats;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acerca_de);

        ButterKnife.bind(this);

        Drawable originalDrawable = getResources().getDrawable(R.drawable.juan);
        Bitmap originalBitmap = ((BitmapDrawable) originalDrawable).getBitmap();

        //creamos el drawable redondeado
        RoundedBitmapDrawable roundedDrawable = RoundedBitmapDrawableFactory.create(getResources(), originalBitmap);

        //asignamos el CornerRadius
        //roundedDrawable.setCornerRadius(originalBitmap.getHeight());
        roundedDrawable.setCircular(true);
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setImageDrawable(roundedDrawable);
    }

    @OnClick(R.id.call)
    public void onClickBtnCall(){
        new AlertDialog.Builder(this)
                .setTitle(R.string.confirmacion)
                .setIcon(R.drawable.ic_priority_high_black_24dp)
                .setMessage(R.string.confirmar_llamar)
                .setCancelable(false)
                .setPositiveButton(R.string.llamar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callTel();
                    }
                })
                .setNegativeButton(R.string.cancelar, null)
                .show();

    }

    public void callTel(){
        try{
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:74251189"));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                startActivity(intent);
            }

        }catch (Exception err){

        }
    }

    @OnClick(R.id.whats)
    public void onClickBtnWhats(){
        Uri uri = Uri.parse("smsto:" + "74251189");
        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
        intent.setPackage("com.whatsapp");
        startActivity(Intent.createChooser(intent, ""));
    }



}
