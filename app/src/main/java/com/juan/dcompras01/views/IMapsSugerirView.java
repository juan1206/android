package com.juan.dcompras01.views;

import com.juan.dcompras01.model.model.Ubicacion;

/**
 * Created by Juan choque on 10/28/2017.
 */

public interface IMapsSugerirView {
    void cargarUbicacion(Ubicacion ubicacion);
    void mostrarProgress(boolean b);
    void mostrarMensaje(String saveIdDb);
    void finalizar();
}
