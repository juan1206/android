package com.juan.dcompras01.views;

import com.juan.dcompras01.model.model.DetalleCompra;
import com.juan.dcompras01.model.model.Producto;

import java.util.List;

/**
 * Created by Juan choque on 10/12/2017.
 */

public interface ICompraAMView {
    void insertarDetalleRecycler(DetalleCompra detalleCompra);
    void cargarProductos(List<Producto> productos);
    void eliminarDetalleRecycler(DetalleCompra detalleCompra);
    void mostrarMensaje(String mensaje);
    void mostrarProgress(boolean mostrar);
    void finalizar();
}
