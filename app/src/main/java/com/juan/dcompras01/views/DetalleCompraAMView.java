package com.juan.dcompras01.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.juan.dcompras01.R;
import com.juan.dcompras01.model.model.DetalleCompra;
import com.juan.dcompras01.presenters.DetalleCompraAMPresenter;
import com.juan.dcompras01.presenters.IDetalleCompraAMPresenter;
import com.juan.dcompras01.presenters.IDetalleCompraPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.Manifest.permission.READ_CONTACTS;

public class DetalleCompraAMView extends AppCompatActivity implements LoaderCallbacks<Cursor>, IDetalleCompraAMView {
    
    @BindView(R.id.descripcion_detalle_compra)
    AutoCompleteTextView txtDescripcion;

    @BindView(R.id.detalle_compra_progress)
    View mProgressView;
    @BindView(R.id.detalle_compra_form)
    View mDetalleCompraFormView;

    private DetalleCompraAMPresenter detalleCompraAMPresenter;
    private DetalleCompra detalleCompra = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_compra_am);

        this.detalleCompraAMPresenter = new DetalleCompraAMPresenter(this.getApplicationContext(),this);

        String idDetalleCompra = null;
        try {
            Bundle bundle = getIntent().getExtras();
            idDetalleCompra = bundle.getString("idDetalleCompra");
        }catch (Exception er){

        }

        ButterKnife.bind(this);

        this.detalleCompraAMPresenter.cargarDetalleCompra(idDetalleCompra);

    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    @Override
    public void mostrarProgress(final boolean show) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mDetalleCompraFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mDetalleCompraFormView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mDetalleCompraFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mDetalleCompraFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI, ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,
                ContactsContract.Contacts.Data.MIMETYPE +  " = ?", new String[]{ContactsContract.CommonDataKinds.Email  .CONTENT_ITEM_TYPE},
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addDescripcionToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addDescripcionToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter = new ArrayAdapter<>(DetalleCompraAMView.this,android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        txtDescripcion.setAdapter(adapter);
    }

    @Override
    public void cargarDetalleCompra(DetalleCompra detalleCompra) {
        if(detalleCompra != null){
            this.detalleCompra = detalleCompra;
            txtDescripcion.setText(detalleCompra.getDescripcion());
        }
    }

    @Override
    public void mostrarMensaje(String mensaje) {
        Snackbar.make(this.mDetalleCompraFormView, mensaje, Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }

    @Override
    public void finalizar() {
        finish();
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    ///////////////////////////////////////////////////////////
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detalle_compra_am, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.save_detalle_compra) {
            //Toast.makeText(this,txtDescripcion.getText().toString(),Toast.LENGTH_SHORT).show();
            this.detalleCompra.setDescripcion(txtDescripcion.getText().toString());
            detalleCompraAMPresenter.guardarDetalleCompra(this.detalleCompra);
            return true;
        }
        if(id == R.id.delete_detalle_compra){
            eliminarDetalleCommpra();
        }

        return super.onOptionsItemSelected(item);
    }

    private void eliminarDetalleCommpra(){
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.confirmacion))
                .setIcon(R.drawable.ic_priority_high_black_24dp)
                .setMessage(getString(R.string.mensaje_eliminar))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.eliminar), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        detalleCompraAMPresenter.eliminarDetalleCompra(detalleCompra);
                    }
                })
                .setNegativeButton(getString(R.string.cancelar), null)
                .show();
    }

}

