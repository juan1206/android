package com.juan.dcompras01.views;

import com.juan.dcompras01.model.model.DetalleCompra;
import com.juan.dcompras01.presenters.adapters.DetalleCompraAdapter;

import java.util.List;

/**
 * Created by Juan choque on 10/12/2017.
 */

public interface IDetalleCompraView {
    void cargarRecycleView(String fecha, DetalleCompraAdapter detalleCompraAdapter);
    void cargarRecycleView(DetalleCompraAdapter detalleCompraAdapter);
    void modificarDetalleCompra(DetalleCompra detalleCompra);
    void buscarEnLista(String txtBuscar);
}
