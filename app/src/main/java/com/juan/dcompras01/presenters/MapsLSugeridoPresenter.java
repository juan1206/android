package com.juan.dcompras01.presenters;

import android.content.Context;

import com.juan.dcompras01.model.manager.UbicacionManager;
import com.juan.dcompras01.model.model.Ubicacion;
import com.juan.dcompras01.views.IMapsLSugeridoView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Juan choque on 10/28/2017.
 */

public class MapsLSugeridoPresenter implements IMapsLSugeridoPresenter {
    private UbicacionManager ubicacionManager;
    private List<Ubicacion>listaUbicaciones;

    private Context context;
    private IMapsLSugeridoView iMapsLSugeridoView;

    public MapsLSugeridoPresenter(Context context, IMapsLSugeridoView iMapsLSugeridoView) {
        this.ubicacionManager = new UbicacionManager();
        this.context = context;
        this.iMapsLSugeridoView = iMapsLSugeridoView;
    }

    @Override
    public void cargarUbicaciones() {
        listaUbicaciones = new ArrayList<Ubicacion>();

        listaUbicaciones = ubicacionManager.listaUbicacions();
        iMapsLSugeridoView.cargarUbicaciones(listaUbicaciones);
    }

    @Override
    public void eliminarUbicacion(String id) {
        Ubicacion ubicacion = new Ubicacion();
        ubicacion.setId(id);
        ubicacionManager.eliminarUbicacion(ubicacion);

        cargarUbicaciones();
    }

    @Override
    public void eliminarUbicacion(Double latitud, Double longitud, String descripcion) {
        ubicacionManager.eliminarUbicacion(latitud, longitud, descripcion);

        cargarUbicaciones();
    }
}
