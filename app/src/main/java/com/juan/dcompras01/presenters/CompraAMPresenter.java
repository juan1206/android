package com.juan.dcompras01.presenters;

import android.content.Context;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.juan.dcompras01.model.manager.CompraManager;
import com.juan.dcompras01.model.manager.DetalleCompraManager;
import com.juan.dcompras01.model.manager.ProductoManager;
import com.juan.dcompras01.model.model.Compra;
import com.juan.dcompras01.model.model.DetalleCompra;
import com.juan.dcompras01.model.model.Producto;
import com.juan.dcompras01.util.ConstantesDcompras;
import com.juan.dcompras01.views.ICompraAMView;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;

/**
 * Created by Juan choque on 10/12/2017.
 */

public class CompraAMPresenter implements  ICompraAMPresenter{
    private Context context;
    private ICompraAMView iCompraAMView;

    private List<Producto>productos;
    private ProductoManager productoManager;

    private CompraTask mAuthTask = null;

    //referencia para recuperar de base de datos
    private IDetalleCompraPresenter iDetalleCompraPresenter;

    public CompraAMPresenter(Context context, ICompraAMView iCompraAMView, IDetalleCompraPresenter iDetalleCompraPresenter) {
        this.context = context;
        this.iCompraAMView = iCompraAMView;
        this.iDetalleCompraPresenter = iDetalleCompraPresenter;
    }

    @Override
    public void insertarProducto(Producto producto, List<DetalleCompra>listaDetalles) {
        //verificar lista de detalles
        boolean existe = false;
        if(producto.getDescripcion().isEmpty()){
            existe = true;
        }
        for(int i = 0;i < listaDetalles.size() && !existe;i++){
            DetalleCompra detalleCompra = listaDetalles.get(i);
            if(producto.getDescripcion().equals(detalleCompra.getDescripcion())){
                existe = true;
                break;
            }
        }

        if(!existe){
            DetalleCompra detalleCompra = new DetalleCompra();
            detalleCompra.setDescripcion(producto.getDescripcion());
            detalleCompra.setProducto(producto);
            iCompraAMView.insertarDetalleRecycler(detalleCompra);
        }

    }

    @Override
    public void eliminarDetalleCompra(DetalleCompra detalleCompra) {
        iCompraAMView.eliminarDetalleRecycler(detalleCompra);
    }

    @Override
    public void guardarCompra(List<DetalleCompra> listaDetalleCompras) {
        if(listaDetalleCompras.size() > 0){
            iCompraAMView.mostrarProgress(true);
            mAuthTask = new CompraTask(listaDetalleCompras);
            mAuthTask.execute((Void) null);
        }
        else{
            iCompraAMView.mostrarMensaje(ConstantesDcompras.NO_LIST_DETALLES);
        }
    }

    @Override
    public void cargarProductos() {
        //lista de productos
        productoManager = new ProductoManager();
        productos = productoManager.listaProductos();

        iCompraAMView.cargarProductos(productos);
    }

    //proceso en segundo plano
    public class CompraTask extends AsyncTask<Void, Void, Boolean> {
        private CompraManager compraManager;
        private DetalleCompraManager detalleCompraManager;
        private ProductoManager productoManager;

        private final List<DetalleCompra>listaDetalles;

        public CompraTask(List<DetalleCompra>listaDetalles) {
            this.listaDetalles = listaDetalles;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Realm realm = Realm.getDefaultInstance();
            this.compraManager = new CompraManager(realm);
            this.detalleCompraManager = new DetalleCompraManager(realm);
            this.productoManager = new ProductoManager(realm);

            boolean result = false;
            try {
                //recuperando la secuencia
                long secuencia = this.compraManager.obtenerSecuenciaMax();

                UUID idCompra = UUID.randomUUID();
                Compra compra = new Compra();
                compra.setId(idCompra.toString());
                compra.setFecha(new Date());
                compra.setSecuencia((secuencia + 1));
                compra.setDescripcion("");
                compra.setEstado("A");
                compra.setCantidad(listaDetalles.size());
                compra.setTotal(0);

                Compra rcompra = compraManager.guardarActualizarCompra(compra);

                if(rcompra != null){
                    for(int i = listaDetalles.size() - 1;i >= 0;i--){
                        secuencia = detalleCompraManager.getSecuenciaMax();

                        UUID idDetalleCompra = UUID.randomUUID();
                        DetalleCompra detalleCompra = listaDetalles.get(i);

                        //verificando producto
                        Producto rproducto = detalleCompra.getProducto();
                        boolean registro = false;
                        if(rproducto.getId() == null){
                            registro = true;
                        }
                        else if(rproducto.getId().isEmpty()){
                            registro = true;
                        }

                        if(registro){
                            UUID idProducto = UUID.randomUUID();
                            rproducto.setId(idProducto.toString());
                            rproducto.setEstado("A");
                            rproducto = productoManager.guardarActualizarProducto(rproducto);
                        }
                        String descripcion = detalleCompra.getDescripcion();
                        descripcion = descripcion.substring(0,1).toUpperCase() + descripcion.substring(1,descripcion.length());
                        detalleCompra.setDescripcion(descripcion);
                        detalleCompra.setId(idDetalleCompra.toString());
                        detalleCompra.setCompra(compra);
                        detalleCompra.setSecuencia((secuencia + 1));
                        detalleCompra.setEstado("A");
                        detalleCompra.setProducto(rproducto);
                        detalleCompraManager.guardarActualizarDetalleCompra(detalleCompra);
                    }
                    result = true;
                }
                else{
                    result = false;
                }
            } catch (Exception e) {
                result = false;
            }
            return result;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            iCompraAMView.mostrarProgress(false);

            if (success) {
                iCompraAMView.mostrarMensaje(ConstantesDcompras.SAVE_ID_DB);
                //actualizar ultima compra en activity principal
                iCompraAMView.finalizar();
            } else {
                iCompraAMView.mostrarMensaje(ConstantesDcompras.NO_SAVE_ID_DB);
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            iCompraAMView.mostrarProgress(false);
        }
    }

}
