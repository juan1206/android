package com.juan.dcompras01.presenters.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.juan.dcompras01.R;
import com.juan.dcompras01.model.model.DetalleCompra;
import com.juan.dcompras01.presenters.CompraDPresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Juan choque on 10/23/2017.
 */

public class CompraDAdapter extends  RecyclerView.Adapter<CompraDAdapter.CardViewHolder>   {
    private final Context mainContext;
    private final List<DetalleCompra> items;
    private CompraDPresenter compraDPresenter;

    public CompraDAdapter(Context mainContext, List<DetalleCompra> items, CompraDPresenter compraDPresenter) {
        this.mainContext = mainContext;
        this.items = items;
        this.compraDPresenter = compraDPresenter;
    }

    @Override
    public CompraDAdapter.CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_detalle_compra_d,parent,false);
        return new CardViewHolder(v,mainContext,items);
    }

    @Override
    public void onBindViewHolder(CompraDAdapter.CardViewHolder holder, int position) {
        final DetalleCompra item = items.get(position); //get item from my List<Place>
        holder.itemView.setTag(item); //save items in Tag

        //set item in cardview respect the position
        holder.fecha.setText(item.getDescripcion());
        holder.cantidad.setText("");
        holder.seleccion.setChecked(true);
        item.setEstado("S");

        holder.seleccion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    item.setEstado("S");
                }
                else {
                    item.setEstado("D");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if(items != null){
            size = items.size();
        }
        return size;
    }


    class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.detalle_d)
        TextView fecha;
        @BindView(R.id.cantidad_d)
        TextView cantidad;
        @BindView(R.id.check_d)
        CheckBox seleccion;

        private Context context;
        private List<DetalleCompra> items = new ArrayList<DetalleCompra>();

        public CardViewHolder(View v, Context context, List<DetalleCompra> items){
            super(v);
            this.context = context;
            this.items = items;

            ButterKnife.bind(this, v); //with butterKnife

            //v.setOnClickListener(this); //click
        }

        //click within RecyclerView
        @Override
        public void onClick(View v) {
            int position= getAdapterPosition();
            DetalleCompra detalleCompra = this.items.get(position);
            ///compraPresenter.modificarCompra(compra);
            //Snackbar.make(v.getRootView(), "Compra Realizada>>" + compra.getDescripcion(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }
    }
}
