package com.juan.dcompras01.presenters.adapters;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;

import com.juan.dcompras01.R;
import com.juan.dcompras01.model.model.DetalleCompra;
import com.juan.dcompras01.presenters.DetalleCompraPresenter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Juan choque on 10/14/2017.
 */

public class DetalleCompraAdapter  extends RecyclerView.Adapter<DetalleCompraAdapter.CardViewHolder>  {
    private final Context mainContext;
    private final List<DetalleCompra> items;
    private DetalleCompraPresenter detalleCompraPresenter;

    public DetalleCompraAdapter(Context mainContext, List<DetalleCompra> items, DetalleCompraPresenter detalleCompraPresenter) {
        this.mainContext = mainContext;
        this.items = items;
        this.detalleCompraPresenter = detalleCompraPresenter;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_detalle_compras,parent,false);
        return new CardViewHolder(v,mainContext,items);
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {
        DetalleCompra item = items.get(position); //get item from my List<Place>
        holder.itemView.setTag(item); //save items in Tag

        //set item in cardview respect the position
        holder.descripcion.setText(item.getDescripcion());
        holder.cantidad.setText("");
        holder.ok.setTag(item);
        holder.ok.setChecked(item.isComprado());

        //agregando eventos
        // Add OnCheckedChangeListener.
        holder.ok.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                DetalleCompra detalleCompra = (DetalleCompra) buttonView.getTag();
                boolean comprado = false;
                if(isChecked){
                    //registro en base
                    //detalleCompra.setComprado(true);
                    comprado = true;
                    Snackbar.make(buttonView.getRootView(), "Compra Realizada", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }
                DetalleCompra ndetalleCompra = new DetalleCompra();
                ndetalleCompra.setId(detalleCompra.getId());
                detalleCompraPresenter.procesarDetalleCompra(ndetalleCompra, buttonView, comprado);
                // Do your stuff here.
            }
        });

        // Add Touch listener.
        holder.ok.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
            return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if(items != null){
            size = items.size();
        }
        return size;
    }

    //class static
    class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.txtdescripcion)
        TextView descripcion;
        @BindView(R.id.txtprecio)
        TextView cantidad;
        @BindView(R.id.switchok)
        Switch ok;

        private Context context;
        private List<DetalleCompra> items = new ArrayList<DetalleCompra>();

        public CardViewHolder(View v, Context context, List<DetalleCompra> items){
            super(v);
            this.context = context;
            this.items = items;
            ButterKnife.bind(this, v); //with butterKnife

            v.setOnClickListener(this); //click
        }

        //click within RecyclerView
        @Override
        public void onClick(View v) {
            int position= getAdapterPosition();
            DetalleCompra detalleCompra = this.items.get(position);
            detalleCompraPresenter.modificarDetalleCompra(detalleCompra);
        }

    }
}
