package com.juan.dcompras01.presenters;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.juan.dcompras01.model.manager.CompraManager;
import com.juan.dcompras01.model.manager.DetalleCompraManager;
import com.juan.dcompras01.model.manager.ProductoManager;
import com.juan.dcompras01.model.model.Compra;
import com.juan.dcompras01.model.model.DetalleCompra;
import com.juan.dcompras01.model.model.Producto;
import com.juan.dcompras01.presenters.adapters.CompraAdapter;
import com.juan.dcompras01.presenters.adapters.CompraDAdapter;
import com.juan.dcompras01.util.ConstantesDcompras;
import com.juan.dcompras01.views.ICompraDView;
import com.juan.dcompras01.views.ICompraView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;

/**
 * Created by Juan choque on 10/12/2017.
 */

public class CompraDPresenter implements ICompraDPresenter{
    private Context context;
    private ICompraDView iCompraDView;
    private CompraManager compraManager;

    private CompraDAdapter compraDAdapter;

    private List<DetalleCompra>listaDetalles;
    private Compra compra;

    private CompraDTask mAuthTask = null;

    public CompraDPresenter(Context context, ICompraDView iCompraDView) {
        this.context = context;
        this.iCompraDView = iCompraDView;
        this.compraManager = new CompraManager();
        this.listaDetalles = new ArrayList<DetalleCompra>();
        this.compra = new Compra();
    }

    @Override
    public void cargarRecyclerView(String idCompra) {
        compra = compraManager.obtenerCompra(idCompra);
        if(compra != null){
            System.out.println("....ANTER DE ADAPTADOR....>" + compra.getListaDetalleCompras().size());
            compraDAdapter = new CompraDAdapter(context, compra.getListaDetalleCompras(), this);
            this.iCompraDView.cargarRecyclerView(compra, compraDAdapter);
        }
    }

    @Override
    public void guardarDetalleCompra() {
        boolean existeSelec = false;

        for(int i = 0;i < compra.getListaDetalleCompras().size();i++){
            DetalleCompra detalleCompra = compra.getListaDetalleCompras().get(i);
            if(detalleCompra.getEstado().equals(ConstantesDcompras.SELECCIONADO)){
                existeSelec = true;
            }
        }
        if(existeSelec){
            List<DetalleCompra>listaDetalles = new ArrayList<DetalleCompra>();
            for(int i = 0;i < compra.getListaDetalleCompras().size();i++){
                DetalleCompra detalleCompra = compra.getListaDetalleCompras().get(i);
                DetalleCompra ndetalleCompra = new DetalleCompra();
                ndetalleCompra.setId(detalleCompra.getId());
                ndetalleCompra.setSecuencia(detalleCompra.getSecuencia());
                ndetalleCompra.setDescripcion(detalleCompra.getDescripcion());
                ndetalleCompra.setFecha(detalleCompra.getFecha());
                ndetalleCompra.setCantidad(detalleCompra.getCantidad());
                ndetalleCompra.setPrecio(detalleCompra.getPrecio());
                ndetalleCompra.setComprado(detalleCompra.isComprado());
                ndetalleCompra.setEstado(detalleCompra.getEstado());

                Producto rproducto = new Producto();
                rproducto.setId(detalleCompra.getProducto().getId());

                ndetalleCompra.setProducto(rproducto);

                listaDetalles.add(ndetalleCompra);
            }

            iCompraDView.mostrarProgress(true);
            mAuthTask = new CompraDTask(listaDetalles);
            mAuthTask.execute((Void) null);
        }
    }


    //proceso en segundo plano
    public class CompraDTask extends AsyncTask<Void, Void, Boolean> {
        private CompraManager compraManager;
        private DetalleCompraManager detalleCompraManager;
        private ProductoManager productoManager;

        private final List<DetalleCompra>listaDetalles;

        public CompraDTask(List<DetalleCompra>listaDetalles) {
            this.listaDetalles = listaDetalles;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Realm realm = Realm.getDefaultInstance();
            this.compraManager = new CompraManager(realm);
            this.detalleCompraManager = new DetalleCompraManager(realm);
            this.productoManager = new ProductoManager(realm);

            boolean result = false;
            try {
                //recuperando la secuencia
                long secuencia = this.compraManager.obtenerSecuenciaMax();

                UUID idCompra = UUID.randomUUID();
                Compra compra = new Compra();
                compra.setId(idCompra.toString());
                compra.setFecha(new Date());
                compra.setSecuencia((secuencia + 1));
                compra.setDescripcion("");
                compra.setEstado("A");
                compra.setCantidad(listaDetalles.size());
                compra.setTotal(0);

                Compra rcompra = compraManager.guardarActualizarCompra(compra);

                if(rcompra != null){
                    for(int i = 0;i < listaDetalles.size();i++){
                        DetalleCompra detalleCompra = listaDetalles.get(i);
                        if(detalleCompra.getEstado().equals(ConstantesDcompras.SELECCIONADO)){
                            secuencia = detalleCompraManager.getSecuenciaMax();
                            UUID idDetalleCompra = UUID.randomUUID();

                            //verificando producto
                            Producto rproducto = productoManager.obtenerProducto(detalleCompra.getProducto().getId());

                            DetalleCompra ndetalleCompra = new DetalleCompra();
                            String descripcion = detalleCompra.getDescripcion();
                            descripcion = descripcion.substring(0,1).toUpperCase() + descripcion.substring(1,descripcion.length());
                            ndetalleCompra.setDescripcion(descripcion);
                            ndetalleCompra.setId(idDetalleCompra.toString());
                            ndetalleCompra.setCompra(rcompra);
                            ndetalleCompra.setSecuencia((secuencia + 1));
                            ndetalleCompra.setEstado("A");
                            ndetalleCompra.setProducto(rproducto);
                            ndetalleCompra.setComprado(false);

                            detalleCompraManager.guardarActualizarDetalleCompra(ndetalleCompra);
                        }
                    }
                    result = true;
                }
                else{
                    result = false;
                }
            } catch (Exception e) {
                Log.i("------>","" + e.getMessage());
                result = false;
            }
            return result;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            iCompraDView.mostrarProgress(false);

            if (success) {
                iCompraDView.mostrarMensaje(ConstantesDcompras.SAVE_ID_DB);
                //actualizar ultima compra en activity principal
                iCompraDView.finalizar();
            } else {
                iCompraDView.mostrarMensaje(ConstantesDcompras.NO_SAVE_ID_DB);
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            iCompraDView.mostrarProgress(false);
        }
    }
}
