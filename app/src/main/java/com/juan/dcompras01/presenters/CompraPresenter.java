package com.juan.dcompras01.presenters;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.juan.dcompras01.model.manager.CompraManager;
import com.juan.dcompras01.model.model.Compra;
import com.juan.dcompras01.presenters.adapters.CompraAdapter;
import com.juan.dcompras01.util.ConstantesDcompras;
import com.juan.dcompras01.util.UtilDcompras;
import com.juan.dcompras01.views.ICompraView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Juan choque on 10/12/2017.
 */

public class CompraPresenter implements ICompraPresenter{
    private ICompraView iCompraView;
    private Context context;
    private List<Compra>listaCompras;
    private CompraAdapter adapter;
    private CompraManager compraManager;

    private UtilDcompras utilDcompras;

    public CompraPresenter(Context context,ICompraView iCompraView) {
        this.context = context;
        this.iCompraView = iCompraView;
        this.compraManager = new CompraManager();
    }

    @Override
    public void cargarRecycleView() {
        this.listaCompras = new ArrayList<Compra>();
        this.listaCompras = this.compraManager.listaCompras();

        adapter = new CompraAdapter(context,listaCompras,this);
        this.iCompraView.cargarRecycleView(adapter);
    }

    @Override
    public void modificarCompra(Compra compra) {
        iCompraView.modificarCompra(compra);
    }

    @Override
    public void buscarEnLista(int anio, int mes, int dia) {
        this.utilDcompras = new UtilDcompras();

        Date fechaInicio = utilDcompras.obtenerFechaDe(anio, mes, dia, ConstantesDcompras.FORMAT_DD_MM_YYYY_HH_MM_SS);
        Date fechaFin = utilDcompras.agregarUnDiaAFecha(fechaInicio);
        
        this.listaCompras = new ArrayList<Compra>();
        this.listaCompras = this.compraManager.listaCompras(fechaInicio, fechaFin);

        adapter = new CompraAdapter(context,listaCompras,this);
        this.iCompraView.cargarRecycleView(adapter);
    }

    /**
     * Proceso en segundo plano
     */
    public class CompraTask extends AsyncTask<Void, Void, Boolean> {

        private final String mDescripcion;
        private final String mCantidad;

        CompraTask(String mDescripcion, String mCantidad) {
            this.mDescripcion = mDescripcion;
            this.mCantidad = mCantidad;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {

        }

        @Override
        protected void onCancelled() {

        }
    }
}
