package com.juan.dcompras01.presenters;

/**
 * Created by Juan choque on 10/28/2017.
 */

public interface IMapsLSugeridoPresenter {
    void cargarUbicaciones();
    void eliminarUbicacion(String id);
    void eliminarUbicacion(Double latitud, Double longitud, String descripcion);
}
