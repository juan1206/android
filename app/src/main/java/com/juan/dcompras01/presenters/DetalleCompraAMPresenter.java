package com.juan.dcompras01.presenters;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.juan.dcompras01.model.manager.CompraManager;
import com.juan.dcompras01.model.manager.DetalleCompraManager;
import com.juan.dcompras01.model.model.Compra;
import com.juan.dcompras01.model.model.DetalleCompra;
import com.juan.dcompras01.util.ConstantesDcompras;
import com.juan.dcompras01.views.IDetalleCompraAMView;

import io.realm.Realm;

/**
 * Created by Juan choque on 10/12/2017.
 */

public class DetalleCompraAMPresenter  implements  IDetalleCompraAMPresenter{
    private Context context;
    private IDetalleCompraAMView iDetalleCompraAMView;

    private DetalleCompraManager detalleCompraManager;

    private DetalleCompraTask mAuthTask = null;

    public DetalleCompraAMPresenter(Context context, IDetalleCompraAMView iDetalleCompraAMView) {
        this.context = context;
        this.iDetalleCompraAMView = iDetalleCompraAMView;
        this.detalleCompraManager = new DetalleCompraManager();
    }

    @Override
    public void cargarDetalleCompra(String idDetalleCompra) {
        DetalleCompra detalleCompra = this.detalleCompraManager.obtenerDetalleCompra(idDetalleCompra);
        iDetalleCompraAMView.cargarDetalleCompra(detalleCompra);
    }

    @Override
    public void guardarDetalleCompra(DetalleCompra detalleCompra) {
        if(detalleCompra != null){
            if(!detalleCompra.getDescripcion().isEmpty()){
                iDetalleCompraAMView.mostrarProgress(true);
                mAuthTask = new DetalleCompraTask(detalleCompra,0);
                mAuthTask.execute((Void) null);
            }
            else{
                iDetalleCompraAMView.mostrarMensaje(ConstantesDcompras.NO_DESCRIPTION);
            }
        }
        else{
            iDetalleCompraAMView.mostrarMensaje(ConstantesDcompras.NO_DESCRIPTION);
        }
    }

    @Override
    public void eliminarDetalleCompra(DetalleCompra detalleCompra) {
        iDetalleCompraAMView.mostrarProgress(true);
        mAuthTask = new DetalleCompraTask(detalleCompra,1);
        mAuthTask.execute((Void) null);
    }

    //proceso en segundo plano
    public class DetalleCompraTask extends AsyncTask<Void, Void, Boolean> {
        private DetalleCompraManager detalleCompraManager;
        private CompraManager compraManager;
        private final DetalleCompra detalleCompra;
        private int sw = 0;

        public DetalleCompraTask(DetalleCompra detalleCompra, int sw) {
            this.detalleCompra = detalleCompra;
            this.sw = sw;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Realm realm = Realm.getDefaultInstance();
            this.detalleCompraManager = new DetalleCompraManager(realm);
            this.compraManager = new CompraManager(realm);

            boolean result = false;
            try {
                DetalleCompra rdetalleCompra = this.detalleCompraManager.obtenerDetalleCompra(detalleCompra.getId());

                if(sw == 0){
                    String descripcion = detalleCompra.getDescripcion();
                    descripcion = descripcion.substring(0,1).toUpperCase() + descripcion.substring(1,descripcion.length());
                    rdetalleCompra.setDescripcion(descripcion);
                    rdetalleCompra.setEstado(detalleCompra.getEstado());

                    detalleCompraManager.guardarActualizarDetalleCompra(rdetalleCompra);
                }
                else if(sw == 1){
                    Compra rcompra = rdetalleCompra.getCompra();
                    detalleCompraManager.eliminarDetalleCompra(rdetalleCompra);

                    if(rcompra != null){
                        int cantidadDetalles = compraManager.obtenerCantidadDetalleCompras(rcompra.getId());
                        Log.i("--------->",">CANTIDAD>" + cantidadDetalles);
                        if(cantidadDetalles == 0){
                            compraManager.eliminarCompra(rcompra);
                        }
                        else{
                            //--cantidadDetalles;
                            Log.i("--------->",">CANTIDAD>" + cantidadDetalles + "<>" + rcompra);
                            Compra rrcompra = compraManager.obtenerCompra(rcompra.getId());
                            if(rrcompra != null){
                                Log.i("--------->",">CANTIDAD>>" + rrcompra);
                                rrcompra.setCantidad((cantidadDetalles > 0)?cantidadDetalles:1);
                                compraManager.guardarActualizarCompra(rrcompra);
                            }

                        }
                    }
                }

                result = true;

            } catch (Exception e) {
                Log.i("--Eliminar rr-->","" + e.getMessage());
                result = false;
            }
            return result;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            iDetalleCompraAMView.mostrarProgress(false);

            if (success) {
                iDetalleCompraAMView.mostrarMensaje(ConstantesDcompras.SAVE_ID_DB);
                //actualizar ultima compra en activity principal
                iDetalleCompraAMView.finalizar();
            } else {
                iDetalleCompraAMView.mostrarMensaje(ConstantesDcompras.NO_SAVE_ID_DB);
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            iDetalleCompraAMView.mostrarProgress(false);
        }
    }
}
