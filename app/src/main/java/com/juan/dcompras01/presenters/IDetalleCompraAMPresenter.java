package com.juan.dcompras01.presenters;

import com.juan.dcompras01.model.model.DetalleCompra;

/**
 * Created by Juan choque on 10/12/2017.
 */

public interface IDetalleCompraAMPresenter {
    void cargarDetalleCompra(String idDetalleCompra);
    void guardarDetalleCompra(DetalleCompra detalleCompra);

    void eliminarDetalleCompra(DetalleCompra detalleCompra);
}

