package com.juan.dcompras01.presenters.adapters;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.juan.dcompras01.R;
import com.juan.dcompras01.model.model.Compra;
import com.juan.dcompras01.model.model.DetalleCompra;
import com.juan.dcompras01.presenters.CompraPresenter;
import com.juan.dcompras01.util.ConstantesDcompras;
import com.juan.dcompras01.util.UtilDcompras;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Juan choque on 10/14/2017.
 */

public class CompraAdapter extends RecyclerView.Adapter<CompraAdapter.CardViewHolder>  {
    private final Context mainContext;
    private final List<Compra> items;
    private UtilDcompras utilDcompras;

    private CompraPresenter compraPresenter;

    public CompraAdapter(Context mainContext, List<Compra> items, CompraPresenter compraPresenter) {
        this.mainContext = mainContext;
        this.items = items;
        this.utilDcompras = new UtilDcompras();
        this.compraPresenter = compraPresenter;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_compras,parent,false);
        return new CardViewHolder(v,mainContext,items);
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {
        Compra item = items.get(position); //get item from my List<Place>
        holder.itemView.setTag(item); //save items in Tag

        //set item in cardview respect the position
        holder.fecha.setText(mainContext.getString(R.string.fecha) + ConstantesDcompras.SPACE + utilDcompras.convertirDateEnString(item.getFecha(), ConstantesDcompras.FORMAT_DD_MM_YYYY_HH_MM_SS));
        holder.cantidad.setText(mainContext.getString(R.string.cant_items) + ConstantesDcompras.SPACE + item.getCantidad() + "");
    }

    @Override
    public int getItemCount() {
        int size = 0;
        if(items != null){
            size = items.size();
        }
        return size;
    }

    //class static
    class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.txtcomprafecha)
        TextView fecha;
        @BindView(R.id.txtcompracantidad)
        TextView cantidad;

        private Context context;
        private List<Compra> items = new ArrayList<Compra>();

        public CardViewHolder(View v, Context context, List<Compra> items){
            super(v);
            this.context = context;
            this.items = items;
            ButterKnife.bind(this, v); //with butterKnife

            v.setOnClickListener(this); //click
        }

        //click within RecyclerView
        @Override
        public void onClick(View v) {
            int position= getAdapterPosition();
            Compra compra = this.items.get(position);
            compraPresenter.modificarCompra(compra);
            //Snackbar.make(v.getRootView(), "Compra Realizada>>" + compra.getDescripcion(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }
    }
}
