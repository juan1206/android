package com.juan.dcompras01.presenters;

/**
 * Created by Juan choque on 10/12/2017.
 */

public interface IPrincipalPresenter {
    public void mostrarMensaje();
}
