package com.juan.dcompras01.presenters;

import com.juan.dcompras01.model.model.DetalleCompra;
import com.juan.dcompras01.model.model.Producto;

import java.util.List;

/**
 * Created by Juan choque on 10/12/2017.
 */

public interface ICompraAMPresenter {
    void insertarProducto(Producto producto, List<DetalleCompra>listaDetalles);
    void eliminarDetalleCompra(DetalleCompra detalleCompra);
    void guardarCompra(List<DetalleCompra> listaDetalleCompras);
    void cargarProductos();
}
