package com.juan.dcompras01.presenters;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.juan.dcompras01.model.manager.UbicacionManager;
import com.juan.dcompras01.model.model.Ubicacion;
import com.juan.dcompras01.util.ConstantesDcompras;
import com.juan.dcompras01.views.IMapsLSugeridoView;
import com.juan.dcompras01.views.IMapsSugerirView;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;

/**
 * Created by Juan choque on 10/28/2017.
 */

public class MapsSugerirPresenter implements IMapsSugerirPresenter{
    private UbicacionManager ubicacionManager;

    private Context context;
    private IMapsSugerirView iMapsLSugeridoView;

    private UbicacionTask mAuthTask = null;


    public MapsSugerirPresenter(Context context, IMapsSugerirView iMapsLSugeridoView) {
        this.context = context;
        this.iMapsLSugeridoView = iMapsLSugeridoView;
        this.ubicacionManager = new UbicacionManager();
    }

    @Override
    public void cargarUbicacion(Ubicacion ubicacion) {

    }

    @Override
    public void guardarUbicacion(Ubicacion ubicacion) {
        iMapsLSugeridoView.mostrarProgress(true);
        mAuthTask = new UbicacionTask(ubicacion);
        mAuthTask.execute((Void) null);
    }

    @Override
    public void eliminarUbicacion(Ubicacion ubicacion) {

    }


    //proceso en segundo plano
    public class UbicacionTask extends AsyncTask<Void, Void, Boolean> {
        private UbicacionManager ubicacionManager;
        private Ubicacion ubicacion;

        public UbicacionTask(Ubicacion ubicacion) {
            this.ubicacion = ubicacion;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Realm realm = Realm.getDefaultInstance();
            this.ubicacionManager = new UbicacionManager(realm);

            boolean result = false;
            try {
                //recuperando la secuencia
                long secuencia = this.ubicacionManager.obtenerSecuenciaMax();

                UUID idUbicacion = UUID.randomUUID();
                Ubicacion nubicacion = new Ubicacion();
                nubicacion.setSecuencia((secuencia + 1));
                nubicacion.setId(idUbicacion.toString());
                nubicacion.setDescripcion(ubicacion.getDescripcion());
                nubicacion.setLogitud(ubicacion.getLogitud());
                nubicacion.setLatitud(ubicacion.getLatitud());
                nubicacion.setEstado("A");

                Ubicacion rubicacion = ubicacionManager.guardarActualizarUbicacion(nubicacion);

                result = true;
            } catch (Exception e) {
                result = false;
            }
            return result;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            iMapsLSugeridoView.mostrarProgress(false);

            if (success) {
                iMapsLSugeridoView.mostrarMensaje(ConstantesDcompras.SAVE_ID_DB);
                //actualizar ultima compra en activity principal
                iMapsLSugeridoView.finalizar();
            } else {
                iMapsLSugeridoView.mostrarMensaje(ConstantesDcompras.NO_SAVE_ID_DB);
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            iMapsLSugeridoView.mostrarProgress(false);
        }
    }
}
