package com.juan.dcompras01.presenters;

import com.juan.dcompras01.model.model.Compra;

import java.util.List;

/**
 * Created by Juan choque on 10/12/2017.
 */

public interface ICompraPresenter {
    void cargarRecycleView();
    void modificarCompra(Compra compra);
    void buscarEnLista(int anio, int mes, int dia);
}
