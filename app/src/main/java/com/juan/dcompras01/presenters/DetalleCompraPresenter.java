package com.juan.dcompras01.presenters;

import android.content.Context;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.widget.CompoundButton;

import com.juan.dcompras01.model.manager.CompraManager;
import com.juan.dcompras01.model.manager.DetalleCompraManager;
import com.juan.dcompras01.model.model.Compra;
import com.juan.dcompras01.model.model.DetalleCompra;
import com.juan.dcompras01.presenters.adapters.DetalleCompraAdapter;
import com.juan.dcompras01.util.ConstantesDcompras;
import com.juan.dcompras01.util.UtilDcompras;
import com.juan.dcompras01.views.IDetalleCompraView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by Juan choque on 10/12/2017.
 */

public class DetalleCompraPresenter implements IDetalleCompraPresenter {
    private IDetalleCompraView iDetalleCompraView;
    private Context context;

    private List<DetalleCompra>listItemsDetalleCompra;
    private DetalleCompraAdapter detalleCompraAdapter;
    private CompraManager compraManager;

    private DetalleCompraTask mAuthTask = null;

    private UtilDcompras utilDcompras;

    public DetalleCompraPresenter(Context context, IDetalleCompraView iDetalleCompraView) {
        this.context = context;
        listItemsDetalleCompra = new ArrayList<DetalleCompra>();
        this.iDetalleCompraView = iDetalleCompraView;

        this.compraManager = new CompraManager();
    }

    @Override
    public void cargarRecycleView() {
        utilDcompras = new UtilDcompras();
        Compra rcompra = compraManager.obtenerUltimaCompra();
        try {
            if(rcompra != null){
                if(rcompra.getListaDetalleCompras() != null){
                    //realizar filtro
                    listItemsDetalleCompra = new ArrayList<DetalleCompra>();
                    for(int i = 0;i < rcompra.getListaDetalleCompras().size();i++){
                        DetalleCompra rdetalleCompra = rcompra.getListaDetalleCompras().get(i);
                        DetalleCompra detalleCompra = new DetalleCompra();
                        detalleCompra.setId(rdetalleCompra.getId());
                        detalleCompra.setFecha(rdetalleCompra.getFecha());
                        detalleCompra.setDescripcion(rdetalleCompra.getDescripcion());
                        detalleCompra.setCantidad(rdetalleCompra.getCantidad());
                        detalleCompra.setSecuencia(rdetalleCompra.getSecuencia());
                        detalleCompra.setEstado(rdetalleCompra.getEstado());
                        detalleCompra.setPrecio(rdetalleCompra.getPrecio());
                        detalleCompra.setProducto(rdetalleCompra.getProducto());
                        detalleCompra.setCompra(rdetalleCompra.getCompra());

                        listItemsDetalleCompra.add(rdetalleCompra);
                    }
                    //Log.i("---------------->",">LISTA DE DETALLES>" + listItemsDetalleCompra.size());
                }
                else{
                    listItemsDetalleCompra = new ArrayList<DetalleCompra>();
                }
            }
            else{
                listItemsDetalleCompra = new ArrayList<DetalleCompra>();
            }
        }catch (Exception err){
        }

        detalleCompraAdapter = new DetalleCompraAdapter(context, listItemsDetalleCompra, this);
        this.iDetalleCompraView.cargarRecycleView((rcompra != null)?utilDcompras.convertirDateEnString(rcompra.getFecha(), ConstantesDcompras.FORMAT_DD_MM_YYYY_HH_MM_SS):"",detalleCompraAdapter);
    }

    @Override
    public void cargarRecycleView(String txtBuscar) {
        List<DetalleCompra>blistaDetalleCompra = new ArrayList<DetalleCompra>();
        if(!txtBuscar.isEmpty()){
            for(int i = 0;i < listItemsDetalleCompra.size();i++){
                DetalleCompra rdetalleCompra = listItemsDetalleCompra.get(i);
                if(rdetalleCompra.getDescripcion().toUpperCase().contains(txtBuscar.toUpperCase())){
                    blistaDetalleCompra.add(rdetalleCompra);
                }
            }
            detalleCompraAdapter = new DetalleCompraAdapter(context, blistaDetalleCompra, this);
            this.iDetalleCompraView.cargarRecycleView(detalleCompraAdapter);
        }
        else{
            detalleCompraAdapter = new DetalleCompraAdapter(context, listItemsDetalleCompra, this);
            this.iDetalleCompraView.cargarRecycleView(detalleCompraAdapter);
        }

    }

    @Override
    public void procesarDetalleCompra(DetalleCompra detalleCompra, CompoundButton compoundButton, boolean comprado) {
        mAuthTask = new DetalleCompraTask(detalleCompra, comprado);
        mAuthTask.execute((Void) null);
    }

    @Override
    public void modificarDetalleCompra(DetalleCompra detalleCompra) {
        iDetalleCompraView.modificarDetalleCompra(detalleCompra);
    }

    //proceso en segundo plano
    public class DetalleCompraTask extends AsyncTask<Void, Void, Boolean> {
        private DetalleCompraManager detalleCompraManager;
        private DetalleCompra detalleCompra;
        private boolean comprado;

        public DetalleCompraTask(DetalleCompra detalleCompra,boolean comprado) {
            this.detalleCompra = detalleCompra;
            this.comprado = comprado;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Realm realm = Realm.getDefaultInstance();
            detalleCompraManager = new DetalleCompraManager(realm);

            boolean result = false;
            try {
                //recuperando la Detalle compra
                DetalleCompra rdetalleCompra = detalleCompraManager.obtenerDetalleCompra(detalleCompra.getId());
                rdetalleCompra.setComprado(comprado);

                DetalleCompra redetalleCompra = detalleCompraManager.guardarActualizarDetalleCompra(rdetalleCompra);

                if(redetalleCompra != null){
                    result = true;
                }
                else{
                    result = false;
                }
            } catch (Exception e) {
                result = false;
            }
            return result;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
        }
    }
}
