package com.juan.dcompras01.presenters;

import com.juan.dcompras01.model.model.Ubicacion;

/**
 * Created by Juan choque on 10/28/2017.
 */

public interface IMapsSugerirPresenter {
    void cargarUbicacion(Ubicacion ubicacion);
    void guardarUbicacion(Ubicacion ubicacion);
    void eliminarUbicacion(Ubicacion ubicacion);
}
