package com.juan.dcompras01.presenters;

import android.widget.CompoundButton;

import com.juan.dcompras01.model.model.DetalleCompra;

/**
 * Created by Juan choque on 10/12/2017.
 */

public interface IDetalleCompraPresenter {
    void cargarRecycleView();
    void cargarRecycleView(String txtBuscar);
    void procesarDetalleCompra(DetalleCompra detalleCompra, CompoundButton compoundButton, boolean comprado);
    void modificarDetalleCompra(DetalleCompra detalleCompra);
}
