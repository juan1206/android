package com.juan.dcompras01.presenters.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.juan.dcompras01.R;
import com.juan.dcompras01.model.model.Producto;

import java.util.ArrayList;

/**
 * Created by Juan choque on 10/17/2017.
 */

public class ProductoAutocompleteAdapter extends ArrayAdapter<Producto> {
    ArrayList<Producto> productos, tempProducto, suggestions ;

    public ProductoAutocompleteAdapter(Context context, ArrayList<Producto> objects) {
        super(context, android.R.layout.simple_list_item_1, objects);
        this.productos = objects ;
        this.tempProducto =new ArrayList<Producto>(objects);
        this.suggestions =new ArrayList<Producto>(objects);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Producto producto = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_autocomplete_producto, parent, false);
        }

        TextView txtCustomer = (TextView) convertView.findViewById(R.id.tvCustomer);
        ImageView ivCustomerImage = (ImageView) convertView.findViewById(R.id.ivCustomerImage);

        if (txtCustomer != null){
            txtCustomer.setText(producto.getDescripcion());
        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return myFilter;
    }

    Filter myFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            Producto producto = (Producto) resultValue;
            return producto.getDescripcion();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (Producto producto : tempProducto) {
                    if (producto.getDescripcion().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        suggestions.add(producto);
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<Producto> c = (ArrayList<Producto>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (Producto cust : c) {
                    add(cust);
                    notifyDataSetChanged();
                }
            }
            else{
                clear();
                notifyDataSetChanged();
            }
        }
    };

}
