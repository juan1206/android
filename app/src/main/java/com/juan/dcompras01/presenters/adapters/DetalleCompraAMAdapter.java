package com.juan.dcompras01.presenters.adapters;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.juan.dcompras01.R;
import com.juan.dcompras01.model.model.DetalleCompra;
import com.juan.dcompras01.presenters.CompraAMPresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Juan choque on 10/14/2017.
 */

public class DetalleCompraAMAdapter extends RecyclerView.Adapter<DetalleCompraAMAdapter.CardViewHolder>  {
    private final Context mainContext;
    private final List<DetalleCompra> items;

    private CompraAMPresenter compraAMPresenter;

    public DetalleCompraAMAdapter(Context mainContext, List<DetalleCompra> items, CompraAMPresenter compraAMPresenter) {
        this.mainContext = mainContext;
        this.items = items;
        this.compraAMPresenter = compraAMPresenter;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_detalle_compra_am,parent,false);
        return new CardViewHolder(v,mainContext,items);
    }

    @Override
    public void onBindViewHolder(final CardViewHolder holder, int position) {
        DetalleCompra item = items.get(position); //get item from my List<Place>
        holder.itemView.setTag(item); //save items in Tag

        //set item in cardview respect the position
        holder.descripcion.setText(item.getDescripcion());
        holder.cantidad.setText("");
        holder.eliminar.setTag(item);

        //agregando eventos
        // Add OnCheckedChangeListener.
        holder.eliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DetalleCompra detalleCompra = (DetalleCompra) view.getTag();
                compraAMPresenter.eliminarDetalleCompra(detalleCompra);
            }
        });

    }

    @Override
    public int getItemCount() {
        int size = 0;
        if(items != null){
            size = items.size();
        }
        return size;
    }

    //class static
    static class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.txtdetalledescripcion)
        TextView descripcion;
        @BindView(R.id.txtdetallecantidad)
        TextView cantidad;
        @BindView(R.id.btndelete)
        ImageButton eliminar;

        private Context context;
        private List<DetalleCompra> items = new ArrayList<DetalleCompra>();

        public CardViewHolder(View v, Context context, List<DetalleCompra> items){
            super(v);
            this.context = context;
            this.items = items;
            ButterKnife.bind(this, v); //with butterKnife

            v.setOnClickListener(this); //click
        }

        //click within RecyclerView
        @Override
        public void onClick(View v) {
            int position= getAdapterPosition();
            DetalleCompra detalleCompra = this.items.get(position);
            Snackbar.make(v.getRootView(), "Compra Realizada>>" + detalleCompra.getDescripcion(), Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }

    }
}
