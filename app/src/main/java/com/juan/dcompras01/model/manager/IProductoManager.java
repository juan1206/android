package com.juan.dcompras01.model.manager;

import com.juan.dcompras01.model.model.Producto;

import java.util.Date;
import java.util.List;

/**
 * Created by Juan choque on 10/12/2017.
 */

public interface IProductoManager {
    public List<Producto> listaProductos();
    public List<Producto> listaProductos(Date fecha);
    public Producto obtenerProducto(String id);
    public Producto guardarActualizarProducto(Producto producto);
    public Producto eliminarProducto(Producto producto);
}
