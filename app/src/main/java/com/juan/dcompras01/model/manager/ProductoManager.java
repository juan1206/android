package com.juan.dcompras01.model.manager;

import com.juan.dcompras01.model.model.Producto;
import com.juan.dcompras01.util.ConstantesDcompras;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Juan choque on 10/12/2017.
 */

public class ProductoManager implements IProductoManager {
    private Realm realm;

    public ProductoManager() {
        this.realm = Realm.getDefaultInstance();
    }

    public ProductoManager(Realm realm) {
        this.realm = realm;
        this.realm = Realm.getDefaultInstance();
    }

    @Override
    public List<Producto> listaProductos() {
        RealmResults<Producto> productos = null;
        List<Producto>listaProductos = new ArrayList<Producto>();
        try {
            productos = realm.where(Producto.class)
                    .equalTo("estado",ConstantesDcompras.ACTIVO)
                    .findAll();
        }catch (Exception er){
            productos = null;
        }

        if(productos != null){
            for(int i = 0;i < productos.size();i++){
                Producto rproducto = productos.get(i);

                Producto producto = new Producto();
                producto.setId(rproducto.getId());
                producto.setDescripcion(rproducto.getDescripcion());
                producto.setEstado(rproducto.getEstado());
                listaProductos.add(producto);
            }
        }

        return listaProductos;
    }

    @Override
    public List<Producto> listaProductos(Date fecha) {
        RealmResults<Producto> productos = null;
        try {
            productos = realm.where(Producto.class)
                    .equalTo("estado",ConstantesDcompras.ACTIVO)
                    .equalTo("fecha",fecha)
                    .findAll();
        }catch (Exception er){
            productos = null;
        }
        return productos;
    }

    @Override
    public Producto obtenerProducto(String id) {
        Producto producto = null;

        try {
            producto = realm.where(Producto.class)
                    .equalTo("id",id)
                    .findFirst();
        }catch (Exception er){

        }

        return producto;
    }

    @Override
    public Producto guardarActualizarProducto(Producto producto) {
        try {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(producto);
            realm.commitTransaction();
        }catch (Exception er){
            producto = null;
        }

        return producto;
    }

    @Override
    public Producto eliminarProducto(Producto producto) {

        try {
            realm.delete(producto.getClass());
        }catch (Exception er){
            producto = null;
        }

        return producto;
    }
}
