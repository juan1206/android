package com.juan.dcompras01.model.manager;

import com.juan.dcompras01.model.model.Compra;

import java.util.Date;
import java.util.List;

/**
 * Created by Juan choque on 10/12/2017.
 */

public interface ICompraManager {
    List<Compra> listaCompras();
    List<Compra> listaCompras(Date fecha);
    Compra obtenerCompra(String id);
    Compra obtenerUltimaCompra();
    Compra guardarActualizarCompra(Compra compra);
    Compra eliminarCompra(Compra compra);

    long obtenerSecuenciaMax();
    int obtenerCantidadDetalleCompras(String id);
    List<Compra> listaCompras(Date fechaInicio, Date fechaFin);
}
