package com.juan.dcompras01.model.manager;

import android.util.Log;
import android.widget.Toast;

import com.juan.dcompras01.model.model.Ubicacion;
import com.juan.dcompras01.util.ConstantesDcompras;

import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by Juan choque on 10/12/2017.
 */

public class UbicacionManager implements IUbicacionManager {
    private Realm realm;

    public UbicacionManager() {
        this.realm = Realm.getDefaultInstance();
    }

    public UbicacionManager(Realm realm) {
        this.realm = realm;
    }

    @Override
    public List<Ubicacion> listaUbicacions() {
        RealmResults<Ubicacion> ubicaciones = null;

        try {
            ubicaciones = realm.where(Ubicacion.class)
                    .equalTo("estado",ConstantesDcompras.ACTIVO)
                    .findAll();
        }catch (Exception er){
            ubicaciones = null;
        }
        return ubicaciones;
    }

    @Override
    public List<Ubicacion> listaUbicacions(Date fecha) {
        RealmResults<Ubicacion> ubicaciones = null;
        try {
            ubicaciones = realm.where(Ubicacion.class)
                    .equalTo("estado",ConstantesDcompras.ACTIVO)
                    .equalTo("fecha",fecha)
                    .findAll();
        }catch (Exception er){
            ubicaciones = null;
        }
        return ubicaciones;
    }

    @Override
    public Ubicacion obtenerUbicacion(String id) {
        Ubicacion ubicacion = null;

        try {
            ubicacion = realm.where(Ubicacion.class)
                    .equalTo("id",id)
                    .findFirst();
        }catch (Exception er){

        }

        return ubicacion;
    }

    @Override
    public Ubicacion guardarActualizarUbicacion(Ubicacion ubicacion) {
        try {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(ubicacion);
            realm.commitTransaction();
        }catch (Exception er){
            ubicacion = null;
        }

        return ubicacion;
    }

    @Override
    public Ubicacion eliminarUbicacion(Ubicacion ubicacion) {

        Ubicacion rubicacion = null;
        try {
            //realm.delete(ubicacion.getClass());
            realm.beginTransaction();
            rubicacion = realm.where(Ubicacion.class)
                    .equalTo("id",ubicacion.getId())
                    .findFirst();

            if(rubicacion != null){
                rubicacion.deleteFromRealm();
                realm.commitTransaction();
            }

        }catch (Exception er){
            ubicacion = null;
        }

        return ubicacion;
    }

    @Override
    public Ubicacion eliminarUbicacion(Double latitud, Double longitud, String descripcion) {

        Ubicacion rubicacion = null;
        try {
            //realm.delete(ubicacion.getClass());
            realm.beginTransaction();
            rubicacion = realm.where(Ubicacion.class)
                    .equalTo("latitud",String.valueOf(latitud))
                    .equalTo("logitud",String.valueOf(longitud))
                    .equalTo("descripcion",descripcion)
                    .findFirst();

            if(rubicacion != null){
                rubicacion.deleteFromRealm();
                realm.commitTransaction();
            }

        }catch (Exception er){
            rubicacion = null;
        }

        return rubicacion;
    }

    @Override
    public long obtenerSecuenciaMax() {
        long result = 0;
        Ubicacion ubicacion = null;

        try {
            ubicacion = realm.where(Ubicacion.class)
                    .findAllSorted("secuencia", Sort.DESCENDING)
                    .first();
        }catch (Exception er){
            ubicacion = null;
        }
        if(ubicacion != null){
            result = ubicacion.getSecuencia();
        }
        return result;
    }
}
