package com.juan.dcompras01.model.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Juan choque on 10/12/2017.
 */

public class Producto extends RealmObject {
    @PrimaryKey
    private String id;
    private String descripcion;
    private double precio;
    private String estado;

    public Producto(String descripcion) {
        this.descripcion = descripcion;
    }

    public Producto() {
    }

    private RealmList<DetalleCompra> detalleCompras = new RealmList<DetalleCompra>();

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public RealmList<DetalleCompra> getDetalleCompras() {
        return detalleCompras;
    }

    public void setDetalleCompras(RealmList<DetalleCompra> detalleCompras) {
        this.detalleCompras = detalleCompras;
    }
}
