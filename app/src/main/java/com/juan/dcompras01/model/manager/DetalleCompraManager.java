package com.juan.dcompras01.model.manager;

import android.util.Log;

import com.juan.dcompras01.model.model.Compra;
import com.juan.dcompras01.model.model.DetalleCompra;
import com.juan.dcompras01.util.ConstantesDcompras;

import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by Juan choque on 10/12/2017.
 */

public class DetalleCompraManager implements IDetalleCompraManager {
    private Realm realm;

    public DetalleCompraManager() {
        this.realm = Realm.getDefaultInstance();
    }

    public DetalleCompraManager(Realm realm) {
        this.realm = realm;
    }

    @Override
    public List<DetalleCompra> listaDetalleCompras() {
        RealmResults<DetalleCompra> detalleCompras = null;

        try {
            detalleCompras = realm.where(DetalleCompra.class)
                    //.equalTo("estado", ConstantesDcompras.ACTIVO)
                    .findAll();
        }catch (Exception er){
            detalleCompras = null;
        }

        return detalleCompras;
    }

    @Override
    public List<DetalleCompra> listaDetalleCompras(Compra compra) {
        RealmResults<DetalleCompra> detalleCompras = null;

        try {
            detalleCompras = realm.where(DetalleCompra.class)
                    .equalTo("compra.id", compra.getId())
                    .findAll();
        }catch (Exception er){
            detalleCompras = null;
        }

        return detalleCompras;
    }

    @Override
    public List<DetalleCompra> listaDetalleCompras(Date fecha) {
        RealmResults<DetalleCompra> detalleCompras = null;
        try {
            detalleCompras = realm.where(DetalleCompra.class)
                    .equalTo("estado",ConstantesDcompras.ACTIVO)
                    .equalTo("fecha",fecha)
                    .findAll();
        }catch (Exception er){
            detalleCompras = null;
        }
        return detalleCompras;
    }

    @Override
    public DetalleCompra obtenerDetalleCompra(String id) {
        DetalleCompra detalleCompra = null;

        try {
            detalleCompra = realm.where(DetalleCompra.class)
                    .equalTo("id",id)
                    .findFirst();
        }catch (Exception er){

        }

        DetalleCompra ndetalleCompra = null;
        if(detalleCompra != null){
            Compra rcompra = detalleCompra.getCompra();

            Compra compra = new Compra();
            compra.setId(rcompra.getId());
            compra.setEstado(rcompra.getEstado());
            compra.setCantidad(rcompra.getCantidad());
            compra.setSecuencia(rcompra.getSecuencia());
            compra.setFecha(rcompra.getFecha());
            compra.setDescripcion(rcompra.getDescripcion());

            ndetalleCompra = new DetalleCompra();
            ndetalleCompra.setCompra(compra);
            ndetalleCompra.setId(detalleCompra.getId());
            ndetalleCompra.setDescripcion(detalleCompra.getDescripcion());
            ndetalleCompra.setEstado(detalleCompra.getEstado());
        }

        return ndetalleCompra;
    }

    @Override
    public DetalleCompra guardarActualizarDetalleCompra(DetalleCompra detalleCompra) {
        try {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(detalleCompra);
            realm.commitTransaction();
        }catch (Exception er){
            Log.i("----->",">>ERRORES AL REGISTRAR>>" + er.getMessage());
            detalleCompra = null;
        }

        return detalleCompra;
    }

    @Override
    public DetalleCompra eliminarDetalleCompra(DetalleCompra detalleCompra) {

        DetalleCompra rdetalleCompra = null;
        try {
            //realm.delete(detalleCompra.getClass());
            rdetalleCompra = realm.where(DetalleCompra.class)
                    .equalTo("id",detalleCompra.getId())
                    .findFirst();

            if(rdetalleCompra != null){
                realm.beginTransaction();
                rdetalleCompra.deleteFromRealm();
                realm.commitTransaction();
            }
        }catch (Exception er){
            detalleCompra = null;
        }

        return detalleCompra;
    }

    @Override
    public long getSecuenciaMax() {
        long result = 0;
        DetalleCompra detalleCompra = null;

        try {
            detalleCompra = realm.where(DetalleCompra.class)
                    .findAllSorted("secuencia", Sort.DESCENDING)
                    .first();
        }catch (Exception er){
            Log.i("----->","ERRORES " + er.getMessage());
            detalleCompra = null;
        }
        if(detalleCompra != null){
            result = detalleCompra.getSecuencia();
        }
        return result;
    }
}
