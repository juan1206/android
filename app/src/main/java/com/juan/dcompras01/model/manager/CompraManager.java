package com.juan.dcompras01.model.manager;


import android.util.Log;

import com.juan.dcompras01.model.model.Compra;
import com.juan.dcompras01.model.model.DetalleCompra;
import com.juan.dcompras01.util.ConstantesDcompras;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by Juan choque on 10/12/2017.
 */

public class CompraManager implements ICompraManager {
    private Realm realm;

    public CompraManager() {
        this.realm = Realm.getDefaultInstance();
    }

    public CompraManager(Realm realm) {
        this.realm = realm;
    }

    @Override
    public List<Compra> listaCompras() {
        RealmResults<Compra> compras = null;
        List<Compra>listaCompras = new ArrayList<Compra>();

        try {
            compras = realm.where(Compra.class)
                    .equalTo("estado", ConstantesDcompras.ACTIVO)
                    .findAllSorted("secuencia",Sort.DESCENDING);
        }catch (Exception er){
            compras = null;
        }

        if(compras != null){
            for(int i = 1;i < compras.size();i++){
                Compra rcompra = compras.get(i);
                listaCompras.add(i - 1,rcompra);
            }
        }

        return listaCompras;
    }

    @Override
    public List<Compra> listaCompras(Date fecha) {
        RealmResults<Compra> compras = null;
        try {
            compras = realm.where(Compra.class)
                    .equalTo("estado",ConstantesDcompras.ACTIVO)
                    .equalTo("fecha",fecha)
                    .findAll();
        }catch (Exception er){
            compras = null;
        }
        return compras;
    }

    @Override
    public Compra obtenerCompra(String id) {
        Compra rcompra = null;//objeto para el retorno

        Compra compra = null;

        try {
            compra = realm.where(Compra.class)
                    .equalTo("id",id)
                    .findFirst();
        }catch (Exception er){
            Log.i("-------->",">>ERRORES AL RECUPERAR COMPRA>>" + er.getMessage());
        }

        if(compra !=  null){
            rcompra = new Compra();
            rcompra.setId(compra.getId());
            rcompra.setFecha(compra.getFecha());
            rcompra.setEstado(compra.getEstado());
            rcompra.setCantidad(compra.getCantidad());
            rcompra.setSecuencia(compra.getSecuencia());

            List<DetalleCompra>rdetalleCompras = null;
            List<DetalleCompra>detalleCompras = null;
            try {
                detalleCompras = realm.where(DetalleCompra.class)
                        .equalTo("compra.id", compra.getId())
                        .findAll();
            }catch (Exception er){
                Log.i("-------->",">>ERRORES AL RECUPERAR EN DETALLE>>" + er.getMessage());
                detalleCompras = null;
            }

            if(detalleCompras != null){
                rdetalleCompras = new ArrayList<DetalleCompra>();

                for(int i = 0;i < detalleCompras.size();i++){
                    DetalleCompra detalleCompra = detalleCompras.get(i);
                    DetalleCompra rdetalleCompra = new DetalleCompra();
                    rdetalleCompra.setId(detalleCompra.getId());
                    rdetalleCompra.setComprado(detalleCompra.isComprado());
                    rdetalleCompra.setEstado(detalleCompra.getEstado());
                    rdetalleCompra.setPrecio(detalleCompra.getPrecio());
                    rdetalleCompra.setCantidad(detalleCompra.getCantidad());
                    rdetalleCompra.setFecha(detalleCompra.getFecha());
                    rdetalleCompra.setProducto(detalleCompra.getProducto());
                    rdetalleCompra.setDescripcion(detalleCompra.getDescripcion());
                    rdetalleCompra.setSecuencia(detalleCompra.getSecuencia());

                    rdetalleCompras.add(rdetalleCompra);
                }
                rcompra.setListaDetalleCompras(rdetalleCompras);
            }
        }

        return rcompra;
    }

    @Override
    public Compra obtenerUltimaCompra() {
        Compra compra = null;
        List<DetalleCompra> detalleCompras = null;
        try {
            compra = realm.where(Compra.class)
                    .equalTo("estado","A")
                    .findAllSorted("secuencia", Sort.DESCENDING)
                    .first();
            //no se debe hacer esto
            detalleCompras = realm.where(DetalleCompra.class)
                    //.contains("detalleCompras.estado","A")
                    .equalTo("estado","A")
                    .contains("compra.id",compra.getId())
                    .findAllSorted("secuencia", Sort.ASCENDING);
            if(compra != null && detalleCompras != null){
                compra.setListaDetalleCompras(detalleCompras);
            }
        }catch (Exception er){
            Log.i("----->",er.getMessage());
            compra = null;
        }
        //Log.i("------->",">COMPRA MANAGER>" + detalleCompras.size() + "<>" + compra.getListaDetalleCompras().size());

        return compra;
    }

    @Override
    public Compra guardarActualizarCompra(Compra compra) {
        try {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(compra);
            realm.commitTransaction();
            Log.i("---CORRECTO--->",">>");
        }catch (Exception er){
            Log.i("---ERROR--->",">>" + er.getMessage());
            compra = null;
        }

        return compra;
    }

    @Override
    public Compra eliminarCompra(Compra compra) {
        Compra rcompra = null;
        try {
            rcompra = realm.where(Compra.class)
                    .equalTo("id",compra.getId())
                    .findFirst();

            if(rcompra != null){
                realm.beginTransaction();
                rcompra.deleteFromRealm();
                realm.commitTransaction();
            }
        }catch (Exception er){
            compra = null;
        }

        return rcompra;
    }

    @Override
    public long obtenerSecuenciaMax() {
        long result = 0;
        Compra compra = null;

        try {
            compra = realm.where(Compra.class)
                    .findAllSorted("secuencia", Sort.DESCENDING)
                    .first();
        }catch (Exception er){
            compra = null;
        }
        if(compra != null){
            result = compra.getSecuencia();
        }
        return result;
    }

    @Override
    public int obtenerCantidadDetalleCompras(String id) {
        int result = 0;
        List<DetalleCompra> detalleCompras = null;

        try {
            detalleCompras = realm.where(DetalleCompra.class)
                    .equalTo("compra.id",id)
                    .findAll();
        }catch (Exception er){
            detalleCompras = null;
        }
        if(detalleCompras != null){
            result = detalleCompras.size();
        }
        return result;
    }

    @Override
    public List<Compra> listaCompras(Date fechaInicio, Date fechaFin) {
        RealmResults<Compra> compras = null;
        Log.i("---FECHAS-->","" + fechaInicio + " " + fechaFin);
        try {
            compras = realm.where(Compra.class)
                    .equalTo("estado",ConstantesDcompras.ACTIVO)
                    .greaterThan("fecha",fechaInicio)
                    .lessThan("fecha", fechaFin)
                    .findAll();
        }catch (Exception er){
            compras = null;
        }
        return compras;
    }
}
