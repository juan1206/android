package com.juan.dcompras01.model.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Juan choque on 10/12/2017.
 */

public class Compra extends RealmObject{
    @PrimaryKey
    private String id;
    private long secuencia;
    private String descripcion;
    private Date fecha;
    private long cantidad;
    private double total;
    private String estado;
    private RealmList<DetalleCompra> detalleCompras = new RealmList<DetalleCompra>();

    @Ignore
    private List<DetalleCompra>listaDetalleCompras = new ArrayList<DetalleCompra>();

    public Compra(Date fecha, long cantidad) {
        this.fecha = fecha;
        this.cantidad = cantidad;
    }

    public Compra(){

    }

    public List<DetalleCompra> getListaDetalleCompras() {
        return listaDetalleCompras;
    }

    public void setListaDetalleCompras(List<DetalleCompra> listaDetalleCompras) {
        this.listaDetalleCompras = listaDetalleCompras;
    }

    public long getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(long secuencia) {
        this.secuencia = secuencia;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public long getCantidad() {
        return cantidad;
    }

    public void setCantidad(long cantidad) {
        this.cantidad = cantidad;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public RealmList<DetalleCompra> getDetalleCompras() {
        return detalleCompras;
    }

    public void setDetalleCompras(RealmList<DetalleCompra> detalleCompras) {
        this.detalleCompras = detalleCompras;
    }
}
