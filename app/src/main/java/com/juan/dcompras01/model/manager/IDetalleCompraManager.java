package com.juan.dcompras01.model.manager;

import com.juan.dcompras01.model.model.Compra;
import com.juan.dcompras01.model.model.DetalleCompra;

import java.util.Date;
import java.util.List;

/**
 * Created by Juan choque on 10/12/2017.
 */

public interface IDetalleCompraManager {
    public List<DetalleCompra> listaDetalleCompras();
    public List<DetalleCompra> listaDetalleCompras(Compra compra);
    public List<DetalleCompra> listaDetalleCompras(Date fecha);
    public DetalleCompra obtenerDetalleCompra(String id);
    public DetalleCompra guardarActualizarDetalleCompra(DetalleCompra detalleCompra);
    public DetalleCompra eliminarDetalleCompra(DetalleCompra detalleCompra);

    public long getSecuenciaMax();
}
