package com.juan.dcompras01.model.manager;

import com.juan.dcompras01.model.model.Ubicacion;

import java.util.Date;
import java.util.List;

/**
 * Created by Juan choque on 10/16/2017.
 */

public interface IUbicacionManager {
    List<Ubicacion> listaUbicacions();
    List<Ubicacion> listaUbicacions(Date fecha);
    Ubicacion obtenerUbicacion(String id);
    Ubicacion guardarActualizarUbicacion(Ubicacion ubicacion);
    Ubicacion eliminarUbicacion(Ubicacion ubicacion);
    Ubicacion eliminarUbicacion(Double latitud, Double longitud, String descripcion);
    long obtenerSecuenciaMax();
}
