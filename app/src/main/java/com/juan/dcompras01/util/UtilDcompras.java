package com.juan.dcompras01.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Juan choque on 10/16/2017.
 */

public class UtilDcompras {
    private SimpleDateFormat simpleDateFormat = null;

    /**
     * Metodo para convertir fecha en cadena
     * @param date
     * @param format
     * @return
     */
    public String convertirDateEnString(Date date, String format){
        String result = "";
        try {
            this.simpleDateFormat = new SimpleDateFormat(format);
            result = simpleDateFormat.format(date);
        }catch (Exception er){

        }
        return result;
    }

    /**
     * Metodo para retornar la fecha a partir de un formato
     * @param anio
     * @param mes
     * @param dia
     * @param format
     * @return
     */
    public Date obtenerFechaDe(int anio, int mes, int dia, String format) {
        Date result = new Date();
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.set(anio,mes,dia);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            result = calendar.getTime();
        }catch (Exception error){
            result = new Date();
        }
        return result;
    }

    /**
     * Metodo para agregr un dia a una fecha determinada
     * @param fecha
     * @return
     */
    public Date agregarUnDiaAFecha(Date fecha){
        Date result = new Date();
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fecha); // Configuramos la fecha que se recibe
            calendar.add(Calendar.DAY_OF_YEAR, 1);  // numero de días a añadir, o restar en caso de días<0
            result = calendar.getTime(); // Devuelve el objeto Date con los nuevos días añadidos
        }catch (Exception error){
            result = new Date();
        }
        return  result;
    }

}
