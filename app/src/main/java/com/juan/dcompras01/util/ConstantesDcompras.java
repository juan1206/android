package com.juan.dcompras01.util;

/**
 * Created by Juan choque on 10/14/2017.
 */

public class ConstantesDcompras {
    public static final String ACTIVO = "A";
    public static final String INACTIVO = "X";
    public static final String FORMAT_DD_MM_YYYY = "dd-MM-yyyy";
    public static final String FORMAT_DD_MM_YYYY_HH_MM_SS = "dd-MM-yyyy HH:mm:ss";
    public static final String SPACE = " ";
    public static String SELECCIONADO = "S";

    public static final String NO_LIST_DETALLES = "DEBE SELECCIONAR AL MENOS UN PRODUCTO PARA REALIZAR ESTA OPERACION";
    public static final String NO_SAVE_ID_DB = "NO SE PUDO REGISTRAR EN EL SISTEMA";
    public static final String SAVE_ID_DB = "COMPRA REGISTRADA EN EL SISTEMA";
    public static final String NO_DESCRIPTION = "DEBE ESCRIBIR LA DESCRIPCON DEL PRODUCTO QUE QUIERE MODIFICAR";
}
